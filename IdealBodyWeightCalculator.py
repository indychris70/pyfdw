## Ideal Body Weight Calculator
##
##BuiltLean.com Ideal Body Weight Formula
##
##It turns out there’s a MUCH better way to calculate your ideal
##weight that takes into account your body fat percentage. Here it is:
##
##Lean Body Mass/(1 – Desired Body Fat Percentage)
##
##where Lean Body Mass (LBM) = Your Body Weight – (Your Body Weight x
##Your Current Body Fat Percentage)

## Function definitions

def calc_lbm(body_weight, current_fat_percentage):
    ''' (number, number) -> number

    Calculates Lean Body Mass: Lean Body Mass is Body Weight – (Body
    Weight x Current Body Fat Percentage).

    >>> calc_lbm(156, .17)
    129.48
    >>> calc_lbm(160, .2)
    128.0

    '''

    LBM = body_weight - (body_weight * current_fat_percentage)
    return LBM


def calc_ideal(LBM, desired_fat_percentage):
    ''' (number, number) -> float

    Calculates a person's ideal weight according to the formula Lean Body Mass/(1 – Desired Body Fat Percentage).

    >>> calc_ideal(129.48, .12)
    147.14
    >>> calc_ideal(128.0, .1)
    142.22

    '''

    ideal_weight = LBM / (1 - desired_fat_percentage)
    return ideal_weight

## Input Your Body Weight

body_weight = float(input('\nEnter your body weight: '))

## Input Your Current Body Fat Percentage

current_fat_percentage = float(input('\nEnter your current body fat percentage: '))

## Calculate Lean Body Mass

LBM = calc_lbm(body_weight, current_fat_percentage)

## Input Your Desired Body Fat Percentage

desired_fat_percentage = float(input('\nEnter your desired body fat percentage: '))

## Caculate Your Ideal Body Weight

ideal_weight = calc_ideal(LBM, desired_fat_percentage)

## Print results

print('\n\nYour Lean Body Mass is ' + str(round(LBM, 2)))

print('\n\nYour Ideal Body Weight is ' + str(round(ideal_weight, 2)))

difference = body_weight - ideal_weight

print('\n\nYou need to lose ' + str(round(difference, 2)) + ' pounds of fat to reach your ideal weight. Good luck, punk.')


