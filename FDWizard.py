## Function Design Wizard, by Chris Pierce
## Last updated 9/15/2013

## Step 0a: Welcome, and credits

print('''

 Welcome to the Function Design Wizard, by Chris Pierce The Function
        Design Wizard gathers input from the user to build and
        display a properly formatted function definition that can be
        copied and pasted into the users module.

        The Function Design Wizard is based on the Function Design
        Recipe by Jennifer Campbell and Paul Gries, University of
        Toronto.

    ''')
## Function definitions

def is_valid_function_name(function_name):
    '''(str) -> bool

    Checks string to be sure that only lowercase letters, numbers and the underscore character are used
    and that the function does not begin with a number. Returns True if that is the case. Otherwise returns
    False. An empty string returns False.

    >>> is_valid_function_name('good_function1')
    True
    >>> is valid_function_name('1_bad_function_name')
    False
    >>> is valid_function_name('A_worse_function_name')
    False
    >>> is_valid_function_name('tHe w0r$T function_name EVAR!!!1!!')
    False

    '''

    begins_with_invalid = False
    invalid_count = 0

    if function_name == '':
        return False

    if function_name[0] not in 'abcdefghijklmnopqrstuvwxyz_':
        begins_with_invalid = True
    
    for i in function_name:
        if i not in 'abcdefghijklmnopqrstuvwxyz_1234567890':
            invalid_count = invalid_count + 1

    if begins_with_invalid or invalid_count > 0:
        return False
    else:
        return True


def is_valid_number_of_examples(str_examples):

    '''(str) -> bool

    Validates input to ensure that only interger values of 1 - 3, inclusive are entered.

    >>> is_valid_number_of_examples(3)
    True
    >>> is_valid_number_of_examples(0)
    False
    is_valid_number_of_examples('a couple')
    False

    '''

    for i in str_examples:
        if i not in '123':
            return False

    number_of_examples = int(str_examples)

    if number_of_examples not in range(4):
        return False

    return True


def create_example(function_name, example_parameter, example_output):
    '''(str, str) -> str

    Takes string input for example paramter(s) and expected example output and formats an example string to
    be later included in the function definition.

    >>> create_example('switch_a_to_b', 'a', 'b')
    '>>> switch_a_to_b("a")\n"b"'
    >>> create_example('average_number', '20, 30', '25')
    '>>> average_number(20, 30)\n25'
    
    '''

    example = function_name + '(' + example_parameter + ')' + '\n' + indent + example_output
    return example



## Step 0b: Indentation

print('-----------------------------------------------------------------------------')

indent_number = input('How many spaces should we indent? ')
indent = ' ' * int(indent_number)

print('-----------------------------------------------------------------------------')

## Step 1a: Name the function

## Need to add an input option to
## list more information about what characters can be used in a valid function name


function_name = input('''



Step 1a: Enter a name for your function.
-----------------------------------------------------------------------------
The name should be short, yet descriptive of what the function does.

You may use lower case letter, numbers, and the underscore character ( _ ).
Your function name must begin with a letter or an underscore.
-----------------------------------------------------------------------------
Enter your function name now: ''')

function_name_valid = is_valid_function_name(function_name)

validated = False

while not validated:
    if function_name_valid:
        print('-----------------------------------------------------------------------------')
        print('You have chosen ' + function_name + ' as the name of your function. You have chosen wisely.')
        validated = True
    else:
        print('-----------------------------------------------------------------------------')
        print('You have chosen ' + function_name + ' as the name of your function. You have chosen poorly.')
        function_name = input('\nChose a new function name: ')
        function_name_valid = is_valid_function_name(function_name)


## Step 1b: Enter Examples

print('''



-----------------------------------------------------------------------------
Step 1b: Enter Examples
-----------------------------------------------------------------------------
Now you will enter some examples of function calls and the expected return values. Your examples should
illustrate (when applicable) how the function operates, test extremes, and handles invalid input. Use as few
examples as possible to accomplish this.

You may list up to 3 examples now, although you may always add more directly to the definition when you
have finished running this wizard.
-----------------------------------------------------------------------------
''')

str_examples = input('How many examples do you want to list? ')

print('-----------------------------------------------------------------------------')

number_of_examples_valid = is_valid_number_of_examples(str_examples)

validated = False

while not validated:
      if not number_of_examples_valid:

          number_of_examples = input('Enter a integer number from 1 to 3, inclusive. ')
          print('-----------------------------------------------------------------------------')
          number_of_examples_valid = is_valid_number_of_examples(number_of_examples)

      else:
      
          validated = True
          example_2_exists = False
          example_3_exists = False
          
          number_of_examples = int(str_examples)
      
          example_paramter = str(input('\nEnter the paramters for your first function example (i.e. what goes inside the paraenthasis): '))
          example_output = str(input('Enter the expected return value for this example. '))
          print('-----------------------------------------------------------------------------')
          example_1 = create_example(function_name, example_paramter, example_output)
      
          if number_of_examples >= 2:
              example_paramter = str(input('\nEnter the paramters for your second function example (i.e. what goes inside the paraenthasis): '))
              example_output = str(input('Enter the expected return value for this example. '))
              print('-----------------------------------------------------------------------------')
              example_2 = create_example(function_name, example_paramter, example_output)
              example_2_exists = True

          if number_of_examples >= 3:
              example_paramter = str(input('\nEnter the paramters for your third function example (i.e. what goes inside the paraenthasis): '))
              example_output = str(input('Enter the expected return value for this example. '))
              print('-----------------------------------------------------------------------------')
              example_3 = create_example(function_name, example_paramter, example_output)
              example_3_exists = True

          


## Step 2: Type Contract
##
##        Example:
##            (number) -> number


print('\n\n\n-----------------------------------------------------------------------------')
print('Step 2: Type Contract')
type_contract = input('''
Enter the type(s) of the parameter(s) separated by commas followed by the type of the return value. Example: (str, int) -> bool

''')




## Step 3: Header
##
##        Example:
##            def convert_to_celsius(fahrenheit):

print('\n\n\n-----------------------------------------------------------------------------')
print('Step 3: Header \n\nExample: def function(parameter1, parameter2): ')
header = input('\nEnter a header for your function definition: ')
if function_name not in header:
    print('-----------------------------------------------------------------------------')
    print('''
    ***********************************************************************
    ***Warning: you must use '+ function_name + ' as your function name.***
    ***********************************************************************
    ''')


## Step 4: Description
##
##        Example:
##            Return the number of Celsius degrees equivalent to fahrenheit degrees.


print('\n\n\n-----------------------------------------------------------------------------')
print('Step 4: Description \n\nExample: Return the number of Celsius degrees equivalent to fahrenheit degrees.: ')
description = input('Enter a description for your function definition: ')



## Step 5: Body
##
##        Example:
##            return (fahrenheit - 32) * 5 / 9
###################


## Step 6: (Encourage) Test(ing)



## Step 7: Output

print('\n\n\n-----------------------------------------------------------------------------')
print('\nThank you for using the Function Design Wizard!\n\nCopy and paste everything below the line into your module. ')
print('-----------------------------------------------------------------------------')

print('\n' + header)
print(indent + "''' " + type_contract)
print('\n' + indent + description)
print('\n' + indent + '>>> ' + example_1)

if example_2_exists:
    print(indent +  '>>> ' + example_2)

if example_3_exists:
    print(indent + '>>> ' + example_3)

print('\n' + indent + "'''")

print('\n' + indent + '{The body of your function definition goes here.')
print(indent + 'Don\'t forget to test your examples!}')





