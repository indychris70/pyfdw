from pygeocoder import Geocoder

def geocode():
    address = input('Enter an address: ')
    results = Geocoder.geocode(address)
    if results.valid_address:
        print('We have a valid address. The type is', str(type(results))+'!')
    else:
        print('That is not a valid address.')

    status=results.status
    print('The status is', status)

    latlong = list(results[0].coordinates)
    lat = latlong[0]
    long = latlong[1]

    count=results.count
    print('The results count is', count)

    street_number=results.street_number
    print('Street Number:', street_number)

    street=results.route
    print('Street Name:', street)

    city=results.locality
    print('City:', city)

    county=results.administrative_area_level_2
    print('County:', county)

    state=results.administrative_area_level_1
    print('State:', state)

    zipcode=results[0].postal_code
    print('Zip Code:', zipcode)

    country=results[0].country
    print('Country:', country)

    formatted_address=results.formatted_address
    print('Formatted Address:', formatted_address)

    location_type=results.location_type
    print('Location Type:', location_type)

    

    
    print('The latitude is', lat, 'and the longitude is', long)
    print('http://www.google.com/maps?q='+str(results[0].coordinates)+'&hl=en&sll='+str(lat)+','+str(long))


## Example

    ##https://maps.google.com/maps?q=(39.897022,+-86.205421)&hl=en&sll=39.779784,-86.13275&sspn=0.471756,1.056747&t=h&z=17

    
    menu()

def menu():
    print('''
Geocode Tool Menu

1. Geocode and address

Press 'q' to quit.

               ''')
    print('Make a selection:')
    menuoption=input()

    if menuoption=='1':
        geocode()
    elif menuoption=='q':
        return False
    else:
        print('Invalid selection. Restarting.')
        menu()

menu()
    




